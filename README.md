## About Pixel

Welcome to Pixel, a website where you can:

```Register and Login:``` Easily sign up and log in to your account.<br />
```Browse Job Listings:``` Check out the latest and featured job opportunities.<br/>
```Search for Jobs:``` Find your ideal job using our search feature.<br/>
```Create and Edit Jobs:``` Employers can post new jobs and edit existing ones.<br/>

![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)
![img_5.png](img_5.png)
![img_6.png](img_6.png)
