<?php

namespace Database\Factories;

use App\Models\Employer;
use App\Models\Job;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Job>
 */
class JobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $salaryAmount = $this->faker->numberBetween(30000, 350000);
        $formattedSalary = '$' . number_format($salaryAmount, 0, '.', ',');

        return [
            'employer_id' => Employer::factory(),
            'title' => fake()->jobTitle(),
            'salary' => $formattedSalary,
            'location' => fake()->randomElement(['Remote', 'Onsite', 'Hybrid']),
            'schedule' => fake()->randomElement(['Full Time', 'Part Time']),
            'url' => fake()->url(),
            'featured' => false,
        ];
    }
}
