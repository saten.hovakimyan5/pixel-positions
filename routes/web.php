<?php

use App\Http\Controllers\JobController;
use App\Http\Controllers\RegisteredUserController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [JobController::class, 'index'])->name('jobs.index');
Route::get('/jobs/create', [JobController::class, 'create'])->name('jobs.create')
    ->middleware('auth');
Route::get('/jobs', [JobController::class, 'showAll'])->name('jobs.showAll')
    ->middleware('auth');
Route::post('/jobs', [JobController::class, 'store'])->name('jobs.store')
    ->middleware('auth');
Route::patch('/jobs/{job}', [JobController::class, 'update'])->name('jobs.update')
    ->middleware('auth')
    ->can('edit', 'job');
Route::get('/jobs/{job}/edit', [JobController::class, 'edit'])->name('jobs.edit')
    ->middleware('auth')
    ->can('edit', 'job');
//    ->can('edit-job', 'job'); Using edit-job Gate in the AppServiceProvider
Route::delete('/jobs/{job}', [JobController::class, 'destroy'])->name('jobs.destroy')
    ->middleware('auth')
    ->can('edit', 'job');
Route::get('jobs/create', [JobController::class, 'create'])->name('jobs.create')
    ->middleware('auth');


Route::get('/search', SearchController::class)->name('search');
Route::get('/tags/{tag:name}', TagController::class)->name('tag');

Route::middleware('guest')->group(function () {
    Route::get('/register', [RegisteredUserController::class, 'create'])->name('register.create');
    Route::post('/register', [RegisteredUserController::class, 'store'])->name('register.store');
    Route::get('/login', [SessionController::class, 'create'])->name('login.create');
    Route::post('/login', [SessionController::class, 'store'])->name('login.store');

});

Route::delete('/logout', [SessionController::class, 'destroy'])->name('login.destroy')
    ->middleware('auth');
