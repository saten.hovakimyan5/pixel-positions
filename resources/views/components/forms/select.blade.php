@props(['label', 'name'])

@php
    $defaults = [
        'id' => $name,
        'name' => $name,
        'class' => "rounded-xl bg-white/10 border border-white/10 text-gray-400 px-5 py-4 w-full focus:ring-blue-500 focus:border-blue-500"
    ];
@endphp

<x-forms.field :$label :$name>
    <select {{ $attributes($defaults) }}>
        {{ $slot }}
    </select>
</x-forms.field>

