<button {{ $attributes->merge(['class' => 'bg-blue-800 rounded-xl py-2 px-6 font-bold']) }}>
    {{ $slot }}
</button>
