@props(['employer', 'width' => 90])
{{--<img src="{{ asset($employer->logo) }}" alt="" class="rounded-xl object-cover" style="width: {{ $width }}px; height: {{ $width }}px;">--}}

@php
    $logoUrl = filter_var($employer->logo, FILTER_VALIDATE_URL) ? $employer->logo : url('storage/' . $employer->logo);
@endphp

<img src="{{ $logoUrl }}" alt="{{ $employer->name }} logo" class="rounded-xl object-cover" style="width: {{ $width }}px; height: {{ $width }}px;">
