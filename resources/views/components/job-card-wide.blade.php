@props(['job', 'editable'=> false])

<x-panel class="flex justify-between gap-x-6 relative">
    <div>
        <x-employer-logo :employer="$job->employer"></x-employer-logo>
    </div>
    <div class="flex-1 flex flex-col">
        <a href="#" class="self-start text-sm text-gray-400">{{ $job->employer->name ?? "Satulya Corporation"}}</a>

        <h3 class="font-bold text-xl mt-3 group-hover:text-blue-800 transition-colors duration-300">
            <a href="{{ $job->url }}" target="_blank">
                {{ $job->title ?? 'Java Backend Developer' }}
            </a>
        </h3>
        <p class="text-sm text-gray-400 mt-auto"> {{$job->salary ?? 'Full Time - From $55,000'}}</p>
    </div>
    <div class="absolute right-2 space-x-1">
        @foreach ($job->tags as $tag)
            <x-tag :tag="$tag"/>
        @endforeach
    </div>
    @if ($editable)
        <div class="flex justify-end mt-auto">
            <x-forms.button>
                <a href="{{ route('jobs.edit', $job) }}">Edit</a>
            </x-forms.button>
        </div>
    @endif
</x-panel>
