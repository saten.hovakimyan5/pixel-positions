<x-layout>
    <section class="pt-8">
        <x-section-heading>
            My Jobs
        </x-section-heading>

        @if ($jobs->isEmpty())
            <p class="text-center mt-6">You have no jobs listed.</p>
        @else
            <div class="mt-6 space-y-6">
                @foreach ($jobs as $job)
                    <x-job-card-wide :editable='true' :job="$job"/>
                @endforeach
            </div>
        @endif
    </section>
</x-layout>
