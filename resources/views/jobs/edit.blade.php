<x-layout>
    <x-page-heading>
        Edit The Job
    </x-page-heading>
    <x-forms.form method="POST" action="/jobs/{{ $job->id }}">
        @method('PATCH')
        <x-forms.input label="Title" name="title" placeholder="{{ $job->title }}"
                       value="{{ old('title', $job->title) }}"/>
        <x-forms.input label="Salary" name="salary" placeholder="{{ $job->salary }}"
                       value="{{ old('salary', $job->salary) }}"/>
        <x-forms.input label="Location" name="location" placeholder="{{ $job->location }}"
                       value="{{ old('location', $job->location) }}"/>

        <x-forms.select label="Schedule" name="schedule">
            <option {{ $job->schedule == 'Full Time' ? 'selected' : '' }}>Full Time</option>
            <option {{ $job->schedule == 'Part Time' ? 'selected' : '' }}>Part Time</option>
        </x-forms.select>
        <x-forms.input label="URL" name="url" placeholder="{{ $job->url }}" value="{{ old('url', $job->url) }}"/>
        <x-forms.checkbox label="Feature (Costs Extra)" name="featured" :checked="$job->featured == 1"/>

        <x-forms.divider/>

        <x-forms.input label="Tags (comma separated)" name="tags" placeholder="backend, mobile, management"
                       value="{{ old('tags', implode(',', $job->tags->pluck('name')->toArray())) }}"/>

        <div class="flex justify-between">
            <x-forms.button type="submit" class="bg-red-800" form="delete-form">Delete</x-forms.button>
            <x-forms.button type="submit">Edit</x-forms.button>
        </div>
    </x-forms.form>

    <form method="POST" action="/jobs/{{ $job->id }}" id="delete-form" class="hidden">
        @csrf
        @method('DELETE')
    </form>
</x-layout>
