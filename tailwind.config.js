/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        // "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                "my-black": "#00001C",
                //"my-black": "#1E1A34",
                "my-dark-blue": "#030630",
                "my": "rgb(27,133,184)",
                "my2": "rgb(30,144,255)",

            },
            fontFamily: {
                "hanken-grotesk": ["Hanken Grotesk", "sans-serif"]
            },
            fontSize: {
                "2xs": ".625rem" //10px
            }
        },
    },
    plugins: [],
}
