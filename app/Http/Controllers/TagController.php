<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\View\View;


class TagController extends Controller
{
    public function __invoke(Tag $tag): View
    {
        $jobs = $tag->jobs()->with(['employer', 'tags'])->get();
        return view('jobs.search', ['jobs' => $jobs]);

    }
}
