<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\View\View;

class SearchController extends Controller
{
    public function __invoke(): View
    {
        $jobs = Job::with(['employer', 'tags'])
            ->where('title', 'LIKE', '%' . request('q') . '%')
            ->get();

        return view('jobs.search', [
            'jobs' => $jobs
        ]);
    }
}
