<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $jobs = Job::latest()->with(['employer', 'tags'])->get()->groupBy('featured');

        return view('jobs.index', [
            'jobs' => $jobs[0],
            'featuredJobs' => $jobs[1],
            'tags' => Tag::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'title' => ['required'],
            'salary' => ['required'],
            'location' => ['required'],
            'schedule' => ['required', Rule::in(['Part Time', 'Full Time'])],
            'url' => ['required', 'active_url'],
            'tags' => ['nullable'],
        ]);

        $attributes['featured'] = $request->has('featured');

        $job = Auth::user()->employer->jobs()->create(Arr::except($attributes, 'tags'));


        if ($attributes['tags'] ?? false) {
            foreach (explode(',', $attributes['tags']) as $tag) {
                $job->tag($tag);
            }
        }

        return redirect('/');
    }

    public function showAll()
    {
        $employer = Auth::user()->employer;

        if ($employer->jobs->count() > 0) {
            $jobs = $employer->jobs;
        } else {
            $jobs = collect();
        }

        return view('jobs.my-jobs', [
            'jobs' => $jobs,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Job $job)
    {
        return view('jobs.edit', [
            'job' => $job,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Job $job)
    {
        //dd(request()->all());
        $attributes = $request->validate([
            'title' => ['nullable', 'string', 'max:255'],
            'salary' => ['nullable', 'string', 'max:255'],
            'location' => ['nullable', 'string', 'max:255'],
            'schedule' => ['nullable', Rule::in(['Part Time', 'Full Time'])],
            'url' => ['nullable', 'active_url'],
            'featured' => ['nullable'],
        ]);

        // Set the 'featured' attribute
        $attributes['featured'] = $request->has('featured');

        // Update the job with validated attributes
        $job->update($attributes);


        if (!empty($request->tags)) {
            // Split tags into an array
            $tags = explode(',', $request->tags);

            // Remove any leading or trailing spaces from each tag
            $tags = array_map('trim', $tags);

            // Sync the tags with the job
            $tagIds = [];
            foreach ($tags as $tagName) {
                $tag = Tag::firstOrCreate(['name' => $tagName]);
                $tagIds[] = $tag->id;
            }
            $job->tags()->sync($tagIds);
        } else {
            // If tags field is empty, detach all tags from the job
            $job->tags()->detach();
        }

        return redirect('/')->with('success', 'Job updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Job $job)
    {
        $job->delete();
        return redirect('/jobs')->with('success', 'Job deleted successfully!');
    }
}
